package de.uniba.wiai.dsg.ajp.assignment1.search.impl;

import de.uniba.wiai.dsg.ajp.assignment1.search.SearchTask;
import de.uniba.wiai.dsg.ajp.assignment1.search.TokenFinder;
import de.uniba.wiai.dsg.ajp.assignment1.search.TokenFinderException;


import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

public class SimpleTokenFinder implements TokenFinder {

	public SimpleTokenFinder() {
		/*
		 * DO NOT REMOVE
		 *
		 * REQUIRED FOR GRADING
		 */
	}

	@Override
	public void search(final SearchTask task) throws TokenFinderException {
		// create the file from the root folder string
		File file = new File(task.getRootFolder());
		// test if the file exists, if not, throw the exception
		if (!file.exists())
			throw new TokenFinderException("The root folder does not exist!");
		// test if the root folder is a directory
		if (!file.isDirectory())
			throw new TokenFinderException("The root folder must be a directory!");
		// create the ignore file object and check if it's valid
		File ignorefile = new File(task.getIgnoreFile());
		if (!ignorefile.exists() || !ignorefile.isFile())
			throw new TokenFinderException("Invalid ignore file specification!");
		// check if file extension is empty
		if (task.getFileExtension().isEmpty())
			throw new TokenFinderException("Please enter file extension!");
		// check if result file is empty
		if (task.getResultFile().isEmpty())
			throw new TokenFinderException("Please enter result file!");
		// check if token is empty
		if(task.getToken().isEmpty())
			throw new TokenFinderException("Please enter token!");
		// read the lines from the ignore file to store which file to ignore
		List<String> ignore = this.readLines(ignorefile);
		// create a print writer to write
		PrintWriter out;
		try {
			out = new PrintWriter(new File(task.getResultFile()));
		} catch (FileNotFoundException e) {
			throw new TokenFinderException(e);
		}
		// look for directories to be ignored, print & write them
		ignoreOut(file, ignore, out);
		// search through the folder
		int tokens = this.searchFolder(file, ignore, task.getFileExtension(), task.getToken(), out);
		// print the total amount of tokens found
		out.println("The project includes **" + task.getToken() + "** " + tokens + " times.");
		System.out.println("The project includes **" + task.getToken() + "** " + tokens + " times.");
		// close the writer
		out.close();
	}

	/**
	 * Reads all lines from the specified file.
	 *
	 * @param file file whose lines should be read
	 * @return read lines
	 */
	private List<String> readLines(File file) throws TokenFinderException {
		try {
			// create a reader object and read all lines
			BufferedReader reader = new BufferedReader(new FileReader(file));
			List<String> lines = reader.lines().collect(Collectors.toList());
			reader.close();
			return lines;
		} catch (IOException e) {
			throw new TokenFinderException(e);
		}
	}

	/**
	 * Checks for directories to be ignored and prints + writes the result.
	 *
	 * @param folder    the folder to be searched
	 * @param ignore    a list of all file names that should be ignored
	 */
	private void ignoreOut(final File folder, final List<String> ignore, final PrintWriter out){
		for (File file : folder.listFiles()) {
			if (ignore.contains(file.getName())) {
				out.println(file + " directory was ignored.\n");
				System.out.println(file + " directory was ignored.\n");
				continue;
			}
			if (file.isDirectory()) {
				// if the file object is a directory, recursively enter the function again
				this.ignoreOut(file, ignore, out);
			}
		}
	}

	/**
	 * Iterates through all files in this folder and outputs found tokens in found
	 * files.
	 *
	 * @param folder    the folder to be searched
	 * @param ignore    a list of all file names that should be ignored
	 * @param extension the extension of the files that should be read and searched
	 * @param token     the token to be searched in the file
	 * @return tokens found
	 */

	private int searchFolder(final File folder, final List<String> ignore, final String extension, final String token,
							 final PrintWriter out) throws TokenFinderException {
		// keep track of found tokens
		int found = 0;
		// iterate over all files in the folder
		for (File file : folder.listFiles()) {
			// if the file should be ignored, print it and continue with the next file
			if (ignore.contains(file.getName())) {
				continue;
			}
			if (file.isFile()) {
				// if the file object is a file
				// if the file does not end with the requested extension, continue with the next
				// one
				if (!file.getName().endsWith(extension))
					continue;
				// otherwise search through the file and store the found tokens
				found += this.searchFile(file, token, out);
			} else if (file.isDirectory()) {
				// if the file object is a directory, increase the amount of found tokens by
				// calling this method recursively and adding the new found amount to the found
				// variable
				found += this.searchFolder(file, ignore, extension, token, out);
			}
		}
		return found;
	}

	/**
	 * Searches through a file for occurrences of the specified token, prints them
	 * if one found and returns the total number of found tokens.
	 *
	 * @param file  file to be searched through
	 * @param token token to be searched for
	 * @param out   output
	 * @return total number of found tokens
	 */

	private int searchFile(final File file, final String token, final PrintWriter out) throws TokenFinderException {
		// keep track of the amount of tokens found
		int found = 0;
		// read the lines from the file
		List<String> lines = this.readLines(file);
		// search line by line
		for (int l = 0; l < lines.size(); l++) {
			// store the line and search for the occurrence
			String line = lines.get(l);
			// keep track of the position in the currently searching line
			int index = -token.length();
			// search for all tokens in the line by finding the first position of a not
			// already found occurrence of the token. If the returned index by
			// String#indexOf return -1, no more tokens was found in this line
			while ((index = line.indexOf(token, index + token.length())) != -1) {
				// increase the found token variable and print the found token
				found++;
				// setting output format for token with ** when found.
				final String output = file.toString() + ":" + (l + 1) + "," + index + "> " + line.substring(0, index) + "**"
						+ token + "**" + line.substring(index + token.length());
				out.println(output);
				System.out.println(output);
			}
		}
		// print the total amount of found tokens and return it
		out.println(file.toString() + " includes **" + token + "** " + found + " times.\n");
		System.out.println(file + " includes **" + token + "** " + found + " times.\n");
		return found;
	}
}